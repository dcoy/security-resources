#!/bin/bash
#
# Secure Kali so you're not a target

function updateKali() {
	apt-get update
	apt-get upgrade
	apt-get dist-upgrade
}

functionchangePW() {
	echo "Update password"
	passwd
}

function updateSSHKeys() {
	local ssh_dir="/etc/ssh"
	mkdir "$ssh_dir/old_keys"
	mv "$ssh_dir/ssh_host_*" "$ssh_dir/old_keys"
	dpkg-reconfigure openssh-server

	md5sum "$ssh_dir/ssh_host_*"
}

function unprivUser() {
	read -p "Enter your username: " username
	adduser $username
	usermod -a -G sudo $username
}

function main() {
	echo "Let's go!"

	changePW
  updateKali
  updateSSHKeys
}