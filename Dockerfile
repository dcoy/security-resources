# Builder stage
FROM golang:1.16.0 as gobuster_builder
WORKDIR /go/src/github.com/OJ/gobuster
RUN git clone --depth=1 https://github.com/OJ/gobuster.git .
RUN go get && go build
RUN make linux

# Tooling image
FROM ubuntu:18.04
RUN apt update
RUN apt install \
    nmap \
    git \
    -y
COPY --from=gobuster_builder /go/bin/gobuster /usr/local/bin/
CMD ["/bin/bash"]
